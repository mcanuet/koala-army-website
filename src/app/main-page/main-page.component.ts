import { Component, OnInit, NgModule } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})

export class MainPageComponent implements OnInit {

  page = {
    title: 'Koala Army',
    subtitle : 'website main page',
    content: 'game download page',
    image: 'assets/images/background-image.jpeg'
  };

  constructor() { }

  ngOnInit() {
  }

}
