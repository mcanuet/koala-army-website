import { Component, OnInit, SecurityContext } from '@angular/core';
// import links from '../../assets/data/link.json';
// import { DomSanitizer } from '@angular/platform-browser';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'demo-page',
  templateUrl: './demo-page.component.html',
  styleUrls: ['./demo-page.component.scss']
})
export class DemoPageComponent implements OnInit {

  // public linksList: {link: string}[] = links;
  // videoLink: string;

  constructor(
    // private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    // const link = this.sanitizer.bypassSecurityTrustUrl(this.linksList[0].link);
    // this.videoLink = link.toString();
  }

}
